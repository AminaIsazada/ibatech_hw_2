package com.company;

import java.util.Random;
import java.util.Scanner;
import java.util.*;
import java.math.*;

public class Main {

    public static void main(String[] args) {
        final char[][] matrix = {
                {'-', '-', '-', '-', '-'},
                {'-', '-', '-', '-', '-'},
                {'-', '-', '-', '-', '-'},
                {'-', '-', '-', '-', '-'},
                {'-', '-', '-', '-', '-'}
        };
        for (int i = 0; i < matrix.length; i++) { //row in matrix
            for (int j = 0; j < matrix[i].length; j++) { //column in each row
                System.out.print(matrix[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println("All set. Get ready to rumble!");

        Random generator = new Random();
        int randomColumn = generator.nextInt(4)+1; //column
        int randomRow = generator.nextInt(4)+1; //row
        //System.out.println(randomColumn);
        //System.out.println(randomRow);

        while (true) {

            Scanner scan = new Scanner(System.in);
            System.out.println("Player, please enter the number for COLUMN: ");
            int inputColumn = scan.nextInt();
            System.out.println("Player, please enter the number for ROW: ");
            int inputRow = scan.nextInt();

            if (inputColumn == randomColumn &&  inputRow ==randomRow) {
                matrix[inputRow-1][inputColumn-1]='x';
                for (int i = 0; i < matrix.length; i++) { //row in matrix
                    for (int j = 0; j < matrix[i].length; j++) { //column in each row
                        System.out.print(matrix[i][j] + " ");
                    }
                    System.out.println();
                }
                break;
            } else {
                matrix[inputRow-1][inputColumn-1]='*';
                for (int i = 0; i < matrix.length; i++) { //row in matrix
                    for (int j = 0; j < matrix[i].length; j++) { //column in each row
                        System.out.print(matrix[i][j] + " ");
                    }
                    System.out.println();
                }
            }
        }
    }
}
